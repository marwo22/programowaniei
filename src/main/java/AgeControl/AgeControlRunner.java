package AgeControl;

public class AgeControlRunner {
    private static int AGE_LIMIT=21;

    public static void main(String[] args) {
        int age = 22;
        boolean result= AgeControl.isAdult(age,AGE_LIMIT);
        System.out.println(result);
    }

    }
