package Tables;

public class Modulo {
    private static int[] table= {2,3,6,9,12,44,41,52,24,45};

    public static void main(String[] args) {
        Modulo.divBy3(table);

    }
    private static int divBy3(int[] table){
        int result= 0;
        for (int i:table){
            if (i%3==0){
                System.out.println(i);
                result += i; //result= result + i
            }

        }
        return result;
    }
}
